--Simple AXI special function register (SFR) used for making AXI4 subordinates easily
--Unaligned and non-word access is not allowed, it should be handled by a wrapper around this entity.

library	IEEE;
	use IEEE.STD_LOGIC_1164.ALL;
	use IEEE.NUMERIC_STD.ALL;
	use IEEE.MATH_REAL.ALL;

library work;
	use work.axi4lite_types.ALL;

entity axi4l_simple_sfr is
	Generic (
			--If true, awaddr / araddr are not compared and the address is assumed to have been decoded before reaching this entity. 
			--Otherwise, they are compared to SFR_ADDR
			EXTERNAL_DECODER : boolean := FALSE;
			A_WIDTH_BITS : integer := 32;
			D_WIDTH_BYTES : integer := 4;

			--Address and Mask of this SFR if address decoding is done (EXTERNAL_DECODER = FALSE)
			SFR_ADDR_MASK : integer := 1023;
			SFR_ADDR : integer := 0;
			SFR_RESET_VALUE : integer := 0);
	Port (
			--S AXI interface
			s_axi_aclk : in std_logic;
			s_axi_aresetn : in std_logic;
			s_axi : inout axi4l(
					aw(
						awaddr(A_WIDTH_BITS - 1 downto 0)),
					ar(
						araddr(A_WIDTH_BITS - 1 downto 0)),
					w(
						wdata(D_WIDTH_BYTES * 8 - 1 downto 0), 
						wstrb(D_WIDTH_BYTES - 1 downto 0)), 
					r(
						rdata(D_WIDTH_BYTES * 8 - 1 downto 0))) := axi4l_32_bus_drive_none;

			dummy : out std_logic);
end axi4l_simple_sfr;

architecture rtl of axi4l_simple_sfr is
	--Lowest actually used bit in SFR_ADDR
	--This cannot be declared in the Generic section because the default is not globally static.
	constant A_LSBITS : integer := integer(ceil(log2(real(D_WIDTH_BYTES))));

	--Dont look at this for too long
	subtype addr_t is std_logic_vector(A_WIDTH_BITS - 1 downto 0);
	subtype decoded_addr_t is std_logic_vector(A_WIDTH_BITS - 1 downto A_LSBITS);
	subtype sfr_data_t is std_logic_vector(D_WIDTH_BYTES * 8 - 1 downto 0);
	constant SLV_ADDR : addr_t := std_logic_vector(to_signed(SFR_ADDR, A_WIDTH_BITS));
	constant SLV_ADDR_MASK : addr_t := std_logic_vector(to_signed(SFR_ADDR_MASK, A_WIDTH_BITS));
	constant SLV_RESET_VALUE : sfr_data_t := std_logic_vector(to_signed(SFR_RESET_VALUE, sfr_data_t'length));

	--Address match signal
	signal araddr_match, awaddr_match : std_logic;

	--Buffered signals
	constant S_AXI_INIT : axi4l_s2m_hs := (awready => '1', arready => '1', others => '0'); 
	signal s_axi_buf : axi4l_s2m_hs := s_axi_init;

	--Accept signals
	signal accept : axi4l_hs;

	--Register itself
	signal sfr : sfr_data_t := SLV_RESET_VALUE;

	--Some other state
	signal write_address_was_bad : std_logic := '0';
begin

	addr_match_comp: if not EXTERNAL_DECODER generate
		--Does this generate the minimum decoding logic?
		araddr_match <= '1' when (s_axi.ar.araddr and SLV_ADDR_MASK) = SLV_ADDR else '0';
		awaddr_match <= '1' when (s_axi.aw.awaddr and SLV_ADDR_MASK) = SLV_ADDR else '0';
	end generate;

	addr_match_always: if EXTERNAL_DECODER generate
		araddr_match <= '1';
		awaddr_match <= '1';
	end generate;

	axi4l_assign_s2m_hs(s_axi, s_axi_buf);
	axi4l_assign_s_accept(accept, s_axi, s_axi_buf);

	process (s_axi_aclk, s_axi_aresetn)
		variable lower : integer;
		variable upper : integer;
	begin
		if s_axi_aresetn = '0' then
			s_axi.r.rdata <= (others => '0');
			s_axi.b.bresp <= "00";
			s_axi.r.rresp <= "00";
			s_axi_buf <= S_AXI_INIT;
			sfr <= SLV_RESET_VALUE;

		elsif rising_edge(s_axi_aclk) then
			--Complete read
			if accept.r = '1' then
				s_axi_buf.rvalid <= '0';
				s_axi_buf.arready <= '1';
			end if;

			--Complete write
			if accept.b = '1' then
				s_axi_buf.bvalid <= '0';
				s_axi_buf.awready <= '1';
			end if;

			--Start write
			if accept.aw = '1' then
				if awaddr_match = '1' then
					s_axi_buf.awready <= '0';
					s_axi_buf.wready <= '1';

					write_address_was_bad <= '0';
				else
					--Error
					write_address_was_bad <= '1';
				end if;
			end if;

			--Write data
			if accept.w = '1' then
				if write_address_was_bad = '1' then
					s_axi_buf.wready <= '0';
					s_axi_buf.bvalid <= '1';

					s_axi.b.bresp <= "10"; --SLVERR
				else
					s_axi_buf.wready <= '0';
					s_axi_buf.bvalid <= '1';

					--Apply write strobes
					for I in D_WIDTH_BYTES - 1 downto 0 loop
						lower := I * 8;
						upper := lower + 7;

						if s_axi.w.wstrb(I) = '1' then
							sfr(upper downto lower) <= s_axi.w.wdata(upper downto lower);
						end if;
					end loop;

					s_axi.b.bresp <= "00"; --OKAY
				end if;
			end if;

			--Start read
			if accept.ar = '1' then
				if araddr_match = '1' then
					s_axi_buf.arready <= '0';
					s_axi_buf.rvalid <= '1';

					s_axi.r.rresp <= "00"; --OKAY
					s_axi.r.rdata <= sfr;
					
				else
					--Error
					s_axi_buf.arready <= '0';
					s_axi_buf.rvalid <= '1';

					s_axi.r.rresp <= "10"; --SLVERR
					s_axi.r.rdata <= (others => '1');
				end if;
			end if;
		end if;
	end process;

end architecture;