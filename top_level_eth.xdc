# 200MHz Sys clock
create_clock -period 5.000 -name sys_clk [get_ports clk_p]
set_property PACKAGE_PIN R4 [get_ports clk_p]
set_property IOSTANDARD DIFF_SSTL15 [get_ports clk_p]

# Forwarded clock for MDIO
#create_generated_clock -name mdic_clk_phy 

# Actual RGMII pin locations (Might break these out to debug reset)
set_property PACKAGE_PIN P14 [get_ports mdio]
set_property IOSTANDARD LVCMOS33 [get_ports mdio]

set_property PACKAGE_PIN N13 [get_ports mdic]
set_property IOSTANDARD LVCMOS33 [get_ports mdic]

set_property PACKAGE_PIN R14 [get_ports phy_rstn]
set_property IOSTANDARD LVCMOS33 [get_ports phy_rstn]
# It doesn't like this, I think false path for reset is only allowed for inputs
set_false_path -to [get_ports phy_rstn]


# Create forwarded clock to PHY
create_generated_clock -name mdic_to_phy_clk -multiply_by 1 -source [get_pins mdic_OBUF_inst/I] [get_ports mdic]

# Setup/Hold for outputs to phy
set_output_delay -clock [get_clocks mdic_to_phy_clk] -max 10.0 [get_ports mdio]
set_output_delay -clock [get_clocks mdic_to_phy_clk] -min -10.0 [get_ports mdio]

# MDIO input delay
set_input_delay -min -clock_fall -clock [get_clocks mdic_to_phy_clk] 0.5 [get_ports mdio]
set_input_delay -max -clock_fall -clock [get_clocks mdic_to_phy_clk] 30.5 [get_ports mdio]

# PHY resetn delay
set_output_delay -max 0.5 [get_ports phy_rstn]
set_output_delay -min 0.0 [get_ports phy_rstn]