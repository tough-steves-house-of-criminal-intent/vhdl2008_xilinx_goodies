--AXI4 to Ethernet MDIO registers

--MAC/PHY clocks:
--125 MHz from MAC to PHY TX datapath (DDR!)
--125 MHz from PHY to MAC RX datapath (DDR!)
--12.5MHz maximum MDIO clock
--Desired Bus Clock > 125MHz recommended (32 bit)

--MIIM access, vaugely SPI

library	IEEE;
	use IEEE.STD_LOGIC_1164.ALL;
	use IEEE.NUMERIC_STD.ALL;
	use IEEE.MATH_REAL.ALL;

library	UNISIM;
	use UNISIM.VCOMPONENTS.ALL;

library XPM;
	use XPM.vcomponents.ALL;

entity axi4_mii_mdio is
	Generic (	MDIO_CLK_HZ : real := 12500000.0);
	Port (	--Ports to PHY
			mdio : inout STD_LOGIC;
			mdic : out STD_LOGIC;
			phy_rstn : out STD_LOGIC;

			mdio_clk : in STD_LOGIC;
			rst : in STD_LOGIC;
			rst_busy : out STD_LOGIC;

			--Clock domain on upstream command interface
			cmd_clk : in STD_LOGIC;

			--MDIO CDC Command
			mdio_reg_addr : in STD_LOGIC_VECTOR(4 downto 0);
			mdio_wdata : in STD_LOGIC_VECTOR(15 downto 0);
			mdio_rdata : out STD_LOGIC_VECTOR(15 downto 0);
			mdio_valid : out STD_LOGIC;
			mdio_start : in STD_LOGIC;
			mdio_we : in STD_LOGIC);
end	axi4_mii_mdio;

architecture Behavioral	of axi4_mii_mdio is
	--Pad signals
	signal mdio_data, mdio_oe, mdio_in : std_logic;

	--Reset busy to reset controller
	signal phy_reset_busy : std_logic;
	
	--Per datasheet
	constant RESET_LOW_TIME_MS : real := 10.0; --ms
	constant RESET_HIGH_TIME_MS : real := 50.0; --ms

	--Data register, shifts out left
	signal mdio_shiftreg : std_logic_vector(31 downto 0);
	
	--Number of shift cycles / shift cycles with OE high remaining
	signal mdio_shiftcount : unsigned(5 downto 0) := (others => '0');
	signal mdio_oe_count : unsigned(5 downto 0) := (others => '0');
	constant total_shifts : unsigned(5 downto 0) := to_unsigned(32, mdio_shiftcount'length);
	constant read_oe_shifts : unsigned(5 downto 0) := to_unsigned(14, mdio_oe_count'length);
	
	--Idle time counter, usually 5 idle bits are required with nobody driving the bus
	constant idle_cycles : unsigned(5 downto 0) := to_unsigned(31, mdio_oe_count'length);
	signal mdio_idle_count : unsigned(5 downto 0) := idle_cycles;
	
	--Fields within MDIO frame
	alias mdio_frame_start is mdio_shiftreg(31 downto 30);
	alias mdio_frame_op is mdio_shiftreg(29 downto 28);
	alias mdio_frame_phy_addr is mdio_shiftreg(27 downto 23);
	alias mdio_frame_phy_reg is mdio_shiftreg(22 downto 18);
	alias mdio_frame_ta is mdio_shiftreg(17 downto 16); --Bus turns around during bits 17 and 16
	alias mdio_frame_data is mdio_shiftreg(15 downto 0);
	
	constant mdio_op_start : std_logic_vector(1 downto 0) := "01";
	constant mdio_op_read : std_logic_vector(1 downto 0) := "10";
	constant mdio_op_write : std_logic_vector(1 downto 0) := "01";
	constant mdio_write_ta : std_logic_vector(1 downto 0) := "10";
	
	type mdio_state_t is (MDIO_START_WRITE, MDIO_START_READ, MDIO_IDLE, MDIO_READY);
	signal mdio_state : mdio_state_t := MDIO_IDLE;

	signal busy_or_idle_time : std_logic;
	signal command_start : std_logic;
	signal start : std_logic;
	signal valid : std_logic := '0';

	--This shouldn't need initialized as they are sync-controlled
	signal wdata : std_logic_vector(15 downto 0);
	signal reg_addr : std_logic_vector(4 downto 0);
	signal we : std_logic;
begin
	--Transaction may start if no previous transaction is still going (or Reset)
	busy_or_idle_time <= '1' when mdio_idle_count > 0 or mdio_shiftcount > 0 else '0';
	command_start <= start and not valid and not busy_or_idle_time;

	--New sync plan
	--Start and Valid use XPM_CDC_SINGLE
	--Other wider signals are CE gated and have multi-cycle constraints:
	--From cmd_clk to mdio_clk: we, wdata, reg_addr
	--From mdio_clk to cmd_clk: rdata

	--Sync for start signal coming in
	sync_hs_in: xpm_cdc_single
	generic map (
		DEST_SYNC_FF => 4,
		SRC_INPUT_REG => 1)
	port map (
		src_clk => cmd_clk,
		src_in => mdio_start,
		dest_clk => mdio_clk,
		dest_out => start);

	--Sync for valid signal going out
	sync_hs_out: xpm_cdc_single
	generic map (
		DEST_SYNC_FF => 4,
		SRC_INPUT_REG => 1)
	port map (
		src_clk => mdio_clk,
		src_in => valid,
		dest_clk => cmd_clk,
		dest_out => mdio_valid);

	--Tri-state buffer for MDIO
	--High T DISABLES the output
	mdio_buf: IOBUF port map (
		I => mdio_data,
		O => mdio_in,
		T => not mdio_oe,
		IO => mdio);
		
	--MDIC
	mdic <= mdio_clk;
	
	--MDIO
	mdio_data <= mdio_shiftreg(mdio_shiftreg'left);

	--Reset
	rst_busy <= phy_reset_busy;
	
	--phy_reset_busy can be treated as an active high reset, but it is only sync-deasserted for mdio_clk
	--Anything other domain needs a reset CDC
	reset_control: entity work.reset_realtime 
	generic map (
		CLK_HZ => MDIO_CLK_HZ,
		RESET_ASSERT_TIME_NS => RESET_LOW_TIME_MS * 1000000.0,
		RESET_RELEASE_TIME_NS => RESET_HIGH_TIME_MS * 1000000.0)
	port map (
		clk => mdio_clk,
		reset_in(0) => rst,
		resetn_out => phy_rstn,
		reset_not_finished => phy_reset_busy);

	--Async process for anything that need safety wrt. async reset
	process (phy_reset_busy)
	begin
		--Enable OE control async
		if mdio_oe_count > 0 then
			mdio_oe <= '1';
		else
			mdio_oe <= '0';
		end if;
	end process;

	process (mdio_clk, phy_reset_busy, mdio_oe_count, valid)
	begin
		--Sync
		--This is ONLY safe because we have ensured a multi-cycle setup and hold for these 3 signals
		--Note we must not reset this because a Start signal could come in on the first cycle out of reset
		--since the synchronizers for start are not reset
		if falling_edge(mdio_clk) then
			wdata <= mdio_wdata;
			we <= mdio_we;
			reg_addr <= mdio_reg_addr;
		end if;

		if phy_reset_busy = '1' then
			--Reset
			valid <= '0';

			mdio_state <= MDIO_IDLE;
			mdio_idle_count <= idle_cycles;
			mdio_shiftcount <= (others => '0');
			mdio_oe_count <= (others => '0');

		elsif falling_edge(mdio_clk) then
			if command_start = '1' then
				mdio_frame_start <= mdio_op_start;
				mdio_frame_phy_addr <= "00000"; --TODO
				mdio_frame_phy_reg <= reg_addr;

				mdio_shiftcount <= total_shifts;

				--Start write?
				if we = '1' then
					--Load Opcode etc
					mdio_frame_op <= mdio_op_write;
					mdio_frame_ta <= mdio_write_ta;
					mdio_frame_data <= wdata;

					mdio_state <= MDIO_START_WRITE;
					mdio_oe_count <= total_shifts;
					
				else
					mdio_frame_op <= mdio_op_read;
					mdio_state <= MDIO_START_READ;
					mdio_oe_count <= read_oe_shifts;

				end if;
			end if;

			--Shift out data
			if mdio_shiftcount > 0 then
				mdio_shiftcount <= mdio_shiftcount - 1;
				mdio_shiftreg <= mdio_shiftreg(mdio_shiftreg'left - 1 downto 0) & mdio_in;
				
				--Last count?
				if mdio_shiftcount = 1 then
					--Without this theres one cycle where you can sneak in an illegal operation
					mdio_idle_count <= idle_cycles;

					--Data should be copied out of the shift reg here
					valid <= '1';

					if mdio_state = MDIO_START_READ then
						mdio_rdata <= mdio_shiftreg(15 downto 0);
					end if;

					mdio_state <= MDIO_IDLE;
				end if;
			end if;
			
			--Count down T cycles
			if mdio_oe_count > 0 then
				mdio_oe_count <= mdio_oe_count - 1;
			end if;

			--Count idle cycles
			if mdio_idle_count > 0 then
				mdio_idle_count <= mdio_idle_count - 1;
			end if;

			--Once start goes low data is accepted
			if start = '0' and valid = '1' then
				valid <= '0';
			end if;

			--Ready again
			if busy_or_idle_time = '0' and start = '0' and valid = '0' then
				mdio_state <= MDIO_READY;
			end if;
		end if;
	end process;
end	Behavioral;
