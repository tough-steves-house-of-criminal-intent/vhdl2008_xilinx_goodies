# Sources
read_vhdl -vhdl2008 axi4lite_types.vhd
read_vhdl -vhdl2008 axi4l_simple_sfr.vhd
read_vhdl -vhdl2008 sfr_test_top.vhd

# IP
read_ip ./.srcs/sources_1/ip/my_jtag/my_jtag.xci

# Constraints
read_xdc top_level_eth.xdc

# Part selection
set_property part xc7a35t-1fgg484 [current_project]