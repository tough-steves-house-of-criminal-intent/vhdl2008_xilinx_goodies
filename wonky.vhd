
library	IEEE;
	use IEEE.STD_LOGIC_1164.ALL;
	use IEEE.NUMERIC_STD.ALL;
	use IEEE.MATH_REAL.ALL;

entity wonky_device is
	port (
		clk : in std_logic;
		enable : in std_logic;
		ready : in std_logic;
		result : out std_logic);
end wonky_device;

architecture rtl of wonky_device is
	signal valid_fe, valid: std_logic := '0';
begin
    result <= valid;

	process (clk, valid)
	begin
	    if valid_fe = '1' and valid = '1' then
	        valid_fe <= '0';
	    end if;
	    
	    if rising_edge(clk) then
	        if valid_fe = '1' then
	            valid <= valid_fe;
	        end if;
	        
	        if ready = '1' and valid = '1' then
	            valid <= '0';
	        end if;
	    end if;
	    
	    if falling_edge(clk) then
			valid_fe <= enable;
		end if;
	end process;
end rtl;