# CDC constraints for axi4_mii_mdio

set bus_clk [get_clocks -of_objects [get_cells we_reg]]
set mdio_clk [get_clocks -of_objects [get_cells mdio_shiftreg*]]

# These can safely be multicycle in the receiving domain, because of the above max-delays
# FAST to SLOW
set_multicycle_path 4 -setup -start -from $bus_clk -to $mdio_clk
set_multicycle_path 3 -hold -from $bus_clk -to $mdio_clk

# SLOW to FAST
set_multicycle_path 4 -setup -from $mdio_clk -to $bus_clk
set_multicycle_path 3 -hold -end -from $mdio_clk -to $bus_clk