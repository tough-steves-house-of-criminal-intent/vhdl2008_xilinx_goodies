library	IEEE;
	use IEEE.STD_LOGIC_1164.ALL;
	use IEEE.NUMERIC_STD.ALL;
	use IEEE.MATH_REAL.ALL;


entity reset_tb is

end reset_tb;

architecture rtl of reset_tb is
	constant RESET_LOW_TIME_MS : real := 10.0;
	constant RESET_HIGH_TIME_MS : real := 15.0;

	signal reset_busy : std_logic;
	signal rstn : std_logic;
	signal reset : std_logic := '0';
	signal clk : std_logic := '1';
begin

	clk <= not clk after 0.5 us;

	reset_control: entity work.reset_realtime 
	generic map (
		CLK_HZ => 1000000.0,
		INPUT_SYNC => TRUE,
		RESET_ASSERT_TIME_NS => RESET_LOW_TIME_MS * 1000000.0,
		RESET_RELEASE_TIME_NS => RESET_HIGH_TIME_MS * 1000000.0)
	port map (
		clk => clk,
		reset_in(0) => reset,
		resetn_out => rstn,
		reset_not_finished => reset_busy);

	process
	begin
		wait for 10 us;

		reset <= '1';

		wait for 1 us;

		reset <= '0';

		wait for 50 ms;
	end process;
end rtl;
