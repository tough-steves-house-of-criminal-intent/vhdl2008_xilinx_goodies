library	IEEE;
	use IEEE.STD_LOGIC_1164.ALL;
	use IEEE.NUMERIC_STD.ALL;

entity mdio_tb is

end	mdio_tb;

architecture rtl of mdio_tb is
	signal mdio, mdic: std_logic;
	signal clk : std_logic := '1';
begin
	clk <= not clk after 40 ns;

	dut: entity work.axi4_mii_mdio port map (
		mdio => mdio,
		mdic => mdic,
		mdio_clk => clk,
		rst => '0',
		rst_busy => open,
		phy_rstn => open);

	process
	begin
		wait for 3000 ns;
	end process;
end rtl;