--AXI4 to Ethernet MDIO registers

--MAC/PHY clocks:
--125 MHz from MAC to PHY TX datapath (DDR!)
--125 MHz from PHY to MAC RX datapath (DDR!)
--12.5MHz maximum MDIO clock
--100 MHz Bus Clock
--200 MHz PHY Ref Clock

--UNSAFE when trying to read and write on the same cycle

library	IEEE;
	use IEEE.STD_LOGIC_1164.ALL;
	use IEEE.NUMERIC_STD.ALL;
	use IEEE.MATH_REAL.ALL;

library	UNISIM;
	use UNISIM.VCOMPONENTS.ALL;

library work;
	use work.axi4lite_types.ALL;

entity gbit_eth_mac is
	Port (	--Clock / Reset
			phy_ref_clk : in STD_LOGIC;
		 	bus_clk : out STD_LOGIC;
		 	rst : in STD_LOGIC;
		
			--PHY Management
			mdio : inout STD_LOGIC;
			mdic : out STD_LOGIC;
			phy_rstn : out STD_LOGIC;

			--AXI interface
			--Control signals
			s_axi_aclk : in STD_LOGIC;
			s_axi_aresetn : in STD_LOGIC;

			--Channels
			s_axi : inout axi4l_32);
end	gbit_eth_mac;

architecture Behavioral	of gbit_eth_mac is
	--Downstream clocks
	signal phy_tx_clk : std_logic;
	signal mdio_clk : std_logic;
	
	--PLL
	signal pll_fb_clk : std_logic;

	--Other clock buffers
	signal phy_tx_clk_drv : std_logic;
	signal mdio_clk_drv : std_logic;
	signal bus_clk_drv : std_logic;

	--Internal reset signals
	signal mdio_rst_busy : std_logic;
	signal delay_control_ready, pll_locked : std_logic;

	--AXI helpers
	constant s_axi_init : axi4l_s2m_hs := (awready => '0', arready => '0', others => '0'); 
	signal s_axi_buf : axi4l_s2m_hs := s_axi_init;
	signal accept : axi4l_hs;

	--When committed we must finish the current read or write before accepting another
	signal committed : std_logic := '0';
	signal bad_write_address : std_logic := '0';

	--MDIO Read/Write
	--new signals
	signal mdio_start : std_logic := '0'; --Start clock crossing transaction
	signal mdio_we : std_logic := '0'; --Write, otherwise read
	signal mdio_valid : std_logic; --Result ready

	--same as old
	signal mdio_reg_addr : std_logic_vector(4 downto 0) := (others => '0');
	signal mdio_wdata : std_logic_vector(15 downto 0) := (others => '0');
	signal mdio_rdata : std_logic_vector(15 downto 0);
	--wdata and rdata are the same
	--writes have no result
	--The transaction starts when mdio_start asserted and cannot be cancelled
	--The transaction ends when mdio_valid asserted
	--Transciever lowers valid after it sees start go low
	--A new transaction can only be started once both start and valid are low
	--we, addr and any wdata must be held until the end of the transaction

	--PHY signal wiring to outputs
	signal mdic_o : std_logic;
begin
	--AXI helpers
	axi4l_assign_s2m_hs(s_axi, s_axi_buf);
	axi4l_assign_s_accept(accept, s_axi, s_axi_buf);

	--ODDR for optimal clock forwarding
	mdic_oddr: ODDR 
	port map (
		C => mdic_o,
		Q => mdic,
		D1 => '1',
		D2 => '0',
		CE => '1',
		R => '0',
		S => '0');


	--Configuration registers
	process (s_axi_aclk, s_axi_aresetn)
	begin
		if s_axi_aresetn = '0' then
			--Async Reset
			s_axi.r.rdata <= x"00000000";
			s_axi.r.rresp <= "00";
			
			s_axi_buf <= s_axi_init;

			mdio_start<= '0';
			committed <= '0';
			bad_write_address <= '0';

		elsif rising_edge(s_axi_aclk) then
			--Someone wants to read or write?
			--Manager is not allowed to cancel if they raise valid, so the assumption valid will be high next cycle is okay
			if s_axi.ar.arvalid = '1' and committed = '0' then
				committed <= '1';
				s_axi_buf.arready <= '1';

			elsif s_axi.aw.awvalid = '1' and committed = '0' then
				committed <= '1';
				s_axi_buf.awready <= '1';

			end if;

			--PHY read
			if accept.ar = '1' then
				--Read reg accepted
				s_axi_buf.arready <= '0';

				--Assume total address space is 1K
				if s_axi.ar.araddr(9 downto 7) = "000" then
					--IEEE Standard Register

					--Start CDC read
					mdio_we <= '0';
					mdio_reg_addr <= s_axi.ar.araddr(6 downto 2);

					mdio_start <= '1';
				else
					--None of that
					s_axi.r.rresp <= "10"; --SLVERR
					s_axi.r.rdata <= X"BADADD03";
					s_axi_buf.rvalid <= '1';
				end if;

			--PHY write
			elsif accept.aw = '1' then
				--Write reg accepted
				s_axi_buf.awready <= '0';
				s_axi_buf.wready <= '1';

				if s_axi.aw.awaddr(9 downto 7) = "000" then
					--IEEE Standard Register
					mdio_reg_addr <= s_axi.aw.awaddr(6 downto 2);
					
					--Strobes would normally be checked but this register area is purely for this type of write
					bad_write_address <= '0';
				else
					--Bad, write will respond with SLVERR
					bad_write_address <= '1';
				end if;
			end if;

			if accept.w = '1' then
				s_axi_buf.wready <= '0';

				if bad_write_address = '1' then
					s_axi.b.bresp <= "10"; --SLVERR
					s_axi_buf.bvalid <= '1';
					committed <= '0';
				else
					--Start CDC write
					mdio_we <= '1';
					mdio_wdata <= s_axi.w.wdata(15 downto 0);

					mdio_start <= '1';
				end if;
			end if;

			--Accept transaction from MDIO
			if mdio_start = '1' and mdio_valid = '1' then
				if mdio_we = '0' then
					--Read complete

					s_axi.r.rdata <= X"0000" & mdio_rdata;

					s_axi.r.rresp <= "00"; --OKAY
					s_axi_buf.rvalid <= '1';
				else
					--Write complete
					
					s_axi.b.bresp <= "00"; --OKAY
					s_axi_buf.bvalid <= '1';
				end if;

				--End transaction
				mdio_start <= '0';
			end if;

			--Accept read/bresponse
			if accept.r = '1' then
				s_axi_buf.rvalid <= '0';
			end if;

			if accept.b = '1' then
				s_axi_buf.bvalid <= '0';
			end if;

			--Finish CDC transaction
			if mdio_start = '0' and mdio_valid = '0' then
				committed <= '0';
			end if;
		end if;
	end process;
 
	--MDIO controller
	mdio_c: entity work.axi4_mii_mdio port map (
		mdio => mdio,
		mdic => mdic_o,
		mdio_clk => mdio_clk,
		cmd_clk => s_axi_aclk,
		rst => '0',
		rst_busy => mdio_rst_busy,
		phy_rstn => phy_rstn,
		mdio_reg_addr => mdio_reg_addr,
		mdio_start => mdio_start,
		mdio_valid => mdio_valid,
		mdio_we => mdio_we,
		mdio_rdata => mdio_rdata,
		mdio_wdata => mdio_wdata);
	
	--RXD requires 2ns clock delay for proper timing
	--Created using an IDELAY
	
	--PLL to create MDIO and PHY clocks from 100MHz bus clock
	--Min fVCO 800MHz
	--Max fVCO 1600MHz (Artix -1)
	--Min PFD 19MHz
	--Max PFD 450MHz

	pll1: PLLE2_BASE 
	generic map (
		BANDWIDTH => "OPTIMIZED",
		CLKFBOUT_MULT => 25, --fPLL = 1250MHz
		CLKFBOUT_PHASE => 0.0,
		CLKIN1_PERIOD  => 5.000,
		CLKOUT0_DIVIDE => 10, --125MHz PHY clock
		CLKOUT1_DIVIDE => 100, --12.5MHz MDIO clock (try 101 to fail timing on purpose!)
		CLKOUT2_DIVIDE => 10, --125MHz bus clock
		DIVCLK_DIVIDE => 4,
		STARTUP_WAIT => "TRUE")
	port map (
		clkin1 => phy_ref_clk,
		clkfbin => pll_fb_clk,
		rst => rst,
		pwrdwn => '0',
		clkfbout => pll_fb_clk, --Allowed?
		clkout0 => phy_tx_clk,
		clkout1 => mdio_clk_drv, --12.5 MHz MDIO clock
		clkout2 => bus_clk_drv,
		locked => pll_locked);

	--It wants a BUFG on these for some reason
	bufg1: BUFG 
	port map (
		I => mdio_clk_drv,
		O => mdio_clk);

	bufg2: BUFG 
	port map (
		I => bus_clk_drv,
		O => bus_clk);

end	Behavioral;
