# Generate Bitstream
synth_design -top eth_mac_test
implement_debug_core
place_design
route_design
write_bitstream -force eth_mac_test.bit