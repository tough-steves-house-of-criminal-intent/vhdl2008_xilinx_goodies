# Sources
read_vhdl -vhdl2008 axi4lite_types.vhd
read_vhdl -vhdl2008 reset_realtime.vhd
read_vhdl -vhdl2008 axi4_mii_mdio.vhd
read_vhdl -vhdl2008 gbit_eth_mac.vhd
read_vhdl -vhdl2008 top.vhd

# Part selection
set_property part xc7a35t-1fgg484 [current_project]

# IP
source my_jtag.tcl
synth_ip [get_ips my_jtag]

# Constraints
read_xdc top_level_eth.xdc

read_xdc axi4_mii_mdio.xdc
set_property SCOPED_TO_REF axi4_mii_mdio [get_files axi4_mii_mdio.xdc]
set_property USED_IN_SYNTHESIS FALSE [get_files axi4_mii_mdio.xdc]
set_property USED_IN_IMPLEMENTATION TRUE [get_files axi4_mii_mdio.xdc]