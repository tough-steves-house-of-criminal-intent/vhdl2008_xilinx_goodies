--Reset module that can be configured for real-time durations to comply with datasheets
library	IEEE;
	use IEEE.STD_LOGIC_1164.ALL;
	use IEEE.NUMERIC_STD.ALL;
	use IEEE.MATH_REAL.ALL;

entity reset_realtime is
	Generic (
			CLK_HZ : real; --Must be specified
			INPUT_SYNC : boolean := FALSE;
			NUMBER_OF_RESETNS : integer := 1;
			NUMBER_OF_RESETS : integer := 1;
			RESET_ASSERT_TIME_NS : real := 10.0; --ns
			RESET_RELEASE_TIME_NS : real := 50.0);
	Port (	--Both can be used
			reset_in : in std_logic_vector(NUMBER_OF_RESETS - 1 downto 0) := (others => '0');
			resetn_in : in std_logic_vector(NUMBER_OF_RESETNS - 1 downto 0) := (others => '1');
			--Both may be used
			reset_out : out std_logic := '1';
			resetn_out : out std_logic := '0';
			--Reset busy
			reset_not_finished : out std_logic;
			reset_finished : out std_logic;
			--Hold to force end of reset, reset counter will pause. 
			--Use for starting a simulation more quickly when reset time is very long, Only!
			finish_early_use_only_for_simulation : in std_logic := '0';
			clk : in std_logic);
end reset_realtime;

architecture rtl of reset_realtime is
	constant RESET_ASSERT_CLKS : integer := integer(ceil((RESET_ASSERT_TIME_NS * CLK_HZ) / 1000000000.0));
	constant RESET_RELEASE_CLKS : integer := integer(ceil((RESET_RELEASE_TIME_NS * CLK_HZ) / 1000000000.0));

	--Reset timer
	subtype reset_counter_range is integer range 0 to RESET_ASSERT_CLKS + RESET_RELEASE_CLKS + 1;
	signal reset_counter : reset_counter_range := 0;

	--Combined reset signals
	signal reset_a : std_logic;
	signal reset : std_logic := '0';

	--Buffers
	signal reset_busy_buf : std_logic := '1';
begin
	--Buffers
	reset_not_finished <= reset_busy_buf;
	reset_finished <= not reset_busy_buf;

	--Combine reset inputs
	process (reset_in, resetn_in)
		variable reset_assert : std_logic;
		variable resetn_assert : std_logic;
	begin
		reset_assert := '0';
		resetn_assert := '0';

		for I in NUMBER_OF_RESETS - 1 downto 0 loop
			reset_assert := reset_assert or reset_in(I);
		end loop;

		for I in NUMBER_OF_RESETNS - 1 downto 0 loop
			resetn_assert := resetn_assert or not resetn_in(I);
		end loop;

		reset_a <= reset_assert or resetn_assert;
	end process;

	--Reset inputs are async
	is_reset_in_async: if not INPUT_SYNC generate
		reset <= reset_a;
	end generate;

	--Run reset counter and control outputs
	process (clk, reset)
	begin
		--Reset inputs are sync
		if INPUT_SYNC then
			if rising_edge(clk) then
				reset <= reset_a;
			end if;
		end if;

		--Async reset
		if reset = '1' then
			reset_busy_buf <= '1';

			--Apply resets
			reset_out <= '1';
			resetn_out <= '0';

			--Start counter
			reset_counter <= 0;
			
		elsif rising_edge(clk) then
			if reset_counter > RESET_ASSERT_CLKS + RESET_RELEASE_CLKS or finish_early_use_only_for_simulation = '1' then
				--Finished
				reset_busy_buf <= '0';
			else
				reset_counter <= reset_counter + 1;
			end if;

			--Assert time up?
			if reset_counter > RESET_ASSERT_CLKS or finish_early_use_only_for_simulation = '1' then
				--Release resets
				reset_out <= '0';
				resetn_out <= '1';
			end if;
		end if;
	end process;
end rtl;