--AXI 4 Lite Record Types
library	IEEE;
	use IEEE.STD_LOGIC_1164.ALL;
	use IEEE.NUMERIC_STD.ALL;

package axi4lite_types is
	type axi4l_channel_aw is record
		awaddr : std_logic_vector;
		awvalid : std_logic;
		awready : std_logic;
		awprot : std_logic_vector(2 downto 0);
	end record axi4l_channel_aw;

	type axi4l_channel_ar is record
		araddr : std_logic_vector;
		arvalid : std_logic;
		arready : std_logic;
		arprot : std_logic_vector(2 downto 0);
	end record axi4l_channel_ar;

	type axi4l_channel_w is record
		wdata : std_logic_vector;
		wvalid : std_logic;
		wready : std_logic;
		wstrb : std_logic_vector;
	end record axi4l_channel_w;

	type axi4l_channel_r is record
		rdata : std_logic_vector;
		rvalid : std_logic;
		rready : std_logic;
		rresp : std_logic_vector(1 downto 0);
	end record axi4l_channel_r;

	type axi4l_channel_b is record
		bresp : std_logic_vector(1 downto 0);
		bvalid : std_logic;
		bready : std_logic;
	end record axi4l_channel_b;

	--Already constrained types for simplicity
	subtype axi4l_channel_aw32 is axi4l_channel_aw(awaddr(31 downto 0));
	subtype axi4l_channel_ar32 is axi4l_channel_ar(araddr(31 downto 0));
	subtype axi4l_channel_w32 is axi4l_channel_w(wdata(31 downto 0), wstrb(3 downto 0));
	subtype axi4l_channel_r32 is axi4l_channel_r(rdata(31 downto 0));

	subtype axi4l_channel_aw64 is axi4l_channel_aw(awaddr(63 downto 0));
	subtype axi4l_channel_ar64 is axi4l_channel_ar(araddr(63 downto 0));
	subtype axi4l_channel_w64 is axi4l_channel_w(wdata(63 downto 0), wstrb(7 downto 0));
	subtype axi4l_channel_r64 is axi4l_channel_r(rdata(63 downto 0));

	--Write only / Read only channel groups
	type axi4l_write_channels is record
		aw : axi4l_channel_aw;
		w : axi4l_channel_w;
		b : axi4l_channel_b;
	end record axi4l_write_channels;

	type axi4l_read_channels is record
		ar : axi4l_channel_ar;
		r : axi4l_channel_r;
	end record axi4l_read_channels;

	--Full interfaces
	type axi4l is record
		aw : axi4l_channel_aw;
		ar : axi4l_channel_ar;
		w : axi4l_channel_w;
		r : axi4l_channel_r;
		b : axi4l_channel_b;
	end record axi4l;

	--Full interfaces with constrained size
	subtype axi4l_32 is axi4l(
		aw(awaddr(31 downto 0)),
		ar(araddr(31 downto 0)),
		w(wdata(31 downto 0), wstrb(3 downto 0)),
		r(rdata(31 downto 0)));

	--Handshake Signals which go from Sub to Manager, and thus might need to be declared as buffer
	type axi4l_s2m_hs is record
		awready : std_logic;
		arready : std_logic;
		wready : std_logic;
		rvalid : std_logic;
		bvalid : std_logic;
	end record axi4l_s2m_hs;

	--Other way
	type axi4l_m2s_hs is record
		awvalid: std_logic;
		arvalid : std_logic;
		wvalid : std_logic;
		rready : std_logic;
		bready : std_logic;
	end record axi4l_m2s_hs;

	--All channel names, for handshake signals etc
	type axi4l_hs is record
		aw : std_logic;
		ar : std_logic;
		w : std_logic;
		r : std_logic;
		b : std_logic;
	end record axi4l_hs;

	--Helpers for testbenches
	constant axi4l_32_bus_drive_none : axi4l_32 := (
		aw => (awaddr => (others => 'Z'), awprot => (others => 'Z'), awvalid => 'Z', awready => 'Z'),
		ar => (araddr => (others => 'Z'), arprot => (others => 'Z'), arvalid => 'Z', arready => 'Z'),
		w => (wvalid => 'Z', wready => 'Z', wdata => (others => 'Z'), wstrb => (others => 'Z')),
		r => (rvalid => 'Z', rready => 'Z', rdata => (others => 'Z'), rresp => (others => 'Z')),
		b => (bvalid => 'Z', bready => 'Z', bresp => "ZZ"));

	constant axi4l_32_bus_idle_m : axi4l_32 := (
		aw => (awaddr => (others => '0'), awprot => (others => '0'), awvalid => '0', awready => 'Z'),
		ar => (araddr => (others => '0'), arprot => (others => '0'), arvalid => '0', arready => 'Z'),
		w => (wvalid => '0', wready => 'Z', wdata => (others => '0'), wstrb => (others => '0')),
		r => (rvalid => 'Z', rready => '0', rdata => (others => 'Z'), rresp => (others => 'Z')),
		b => (bvalid => 'Z', bready => '0', bresp => "ZZ"));

	--Assign S to M handshake signals, use to connect axi4l_s2m_hs to axi4lite interface in one shot
	--Make sure to use the "S" version of this procedure on the Subordinate and "M" on the Manager
	procedure axi4l_assign_s2m_hs(signal axi : out axi4l; signal s2m_hs : in axi4l_s2m_hs);
	procedure axi4l_assign_m2s_hs(signal axi : out axi4l; signal m2s_hs : in axi4l_m2s_hs);

	--Get "Accept" signals (xready and xvalid) for each channel
	--Make sure to use the "S" version of this procedure on the Subordinate and "M" on the Manager
	procedure axi4l_assign_s_accept(signal hs : out axi4l_hs; signal axi_m_in : in axi4l; signal axi_s_bufs : in axi4l_s2m_hs);
	procedure axi4l_assign_m_accept(signal hs : out axi4l_hs; signal axi_s_in : in axi4l; signal axi_m_bufs : in axi4l_m2s_hs);

end package axi4lite_types;

package body axi4lite_types is
	
	--I dream of VHDL 2018
	procedure axi4l_assign_s2m_hs(signal axi : out axi4l; signal s2m_hs : in axi4l_s2m_hs) is
	begin
		axi.aw.awready <= s2m_hs.awready;
		axi.ar.arready <= s2m_hs.arready;
		axi.w.wready <= s2m_hs.wready;
		axi.r.rvalid <= s2m_hs.rvalid;
		axi.b.bvalid <= s2m_hs.bvalid;
	end procedure;

	procedure axi4l_assign_m2s_hs(signal axi : out axi4l; signal m2s_hs : in axi4l_m2s_hs) is
	begin
		axi.aw.awvalid <= m2s_hs.awvalid;
		axi.ar.arvalid <= m2s_hs.arvalid;
		axi.w.wvalid <= m2s_hs.wvalid;
		axi.r.rready <= m2s_hs.rready;
		axi.b.bready <= m2s_hs.bready;
	end procedure;
	
	procedure axi4l_assign_s_accept(signal hs : out axi4l_hs; signal axi_m_in : in axi4l; signal axi_s_bufs : in axi4l_s2m_hs) is
	begin
		hs.aw <= axi_s_bufs.awready and axi_m_in.aw.awvalid;
		hs.ar <= axi_s_bufs.arready and axi_m_in.ar.arvalid;
		hs.r <= axi_m_in.r.rready and axi_s_bufs.rvalid;
		hs.b <= axi_m_in.b.bready and axi_s_bufs.bvalid;
		hs.w <= axi_s_bufs.wready and axi_m_in.w.wvalid;
	end procedure;

	procedure axi4l_assign_m_accept(signal hs : out axi4l_hs; signal axi_s_in : in axi4l; signal axi_m_bufs : in axi4l_m2s_hs) is
	begin
		hs.aw <= axi_s_in.aw.awready and axi_m_bufs.awvalid;
		hs.ar <= axi_s_in.ar.arready and axi_m_bufs.arvalid;
		hs.r <= axi_m_bufs.rready and axi_s_in.r.rvalid;
		hs.b <= axi_m_bufs.bready and axi_s_in.b.bvalid;
		hs.w <= axi_s_in.w.wready and axi_m_bufs.wvalid;
	end procedure;

end package body axi4lite_types;