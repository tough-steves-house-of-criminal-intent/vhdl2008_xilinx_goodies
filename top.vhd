library	IEEE;
	use IEEE.STD_LOGIC_1164.ALL;
	use IEEE.NUMERIC_STD.ALL;

library	UNISIM;
	use UNISIM.VCOMPONENTS.ALL;

library work;
	use work.axi4lite_types.ALL;

entity eth_mac_test is
	Port (	--clk_ref pads
			clk_p	: in STD_LOGIC;
			clk_n	: in STD_LOGIC;
		
			--Gigabit Ethernet PHY
			mdio : inout STD_LOGIC;
			mdic : out STD_LOGIC;
			phy_rstn : out STD_LOGIC);
end	eth_mac_test;

architecture Behavioral	of eth_mac_test is
	--200MHz PHY Ref / IDELAY clk
	--125MHz RGMII TX clk is derrived from this
	signal phy_ref_clk : std_logic;

	--100MHz Bus clock, synchronous to phy_ref
	signal bus_clk : std_logic;
	signal aresetn:  STD_LOGIC;

	signal m_axi : axi4l_32;
begin
	--Keep out of reset
	aresetn <= '1';

	i_my_jtag : entity work.my_jtag
		port map (
			aclk          => bus_clk     ,
			aresetn       => aresetn  	  ,
			m_axi_awprot  => m_axi.aw.awprot ,
			m_axi_awaddr  => m_axi.aw.awaddr ,
			m_axi_awvalid => m_axi.aw.awvalid,
			m_axi_awready => m_axi.aw.awready,
			m_axi_wdata   => m_axi.w.wdata  ,
			m_axi_wstrb   => m_axi.w.wstrb  ,
			m_axi_wvalid  => m_axi.w.wvalid ,
			m_axi_wready  => m_axi.w.wready ,
			m_axi_bresp   => m_axi.b.bresp  ,
			m_axi_bvalid  => m_axi.b.bvalid ,
			m_axi_bready  => m_axi.b.bready ,
			m_axi_araddr  => m_axi.ar.araddr ,
			m_axi_arprot  => m_axi.ar.arprot ,
			m_axi_arvalid => m_axi.ar.arvalid,
			m_axi_arready => m_axi.ar.arready,
			m_axi_rdata   => m_axi.r.rdata  ,
			m_axi_rresp   => m_axi.r.rresp  ,
			m_axi_rvalid  => m_axi.r.rvalid ,
			m_axi_rready  => m_axi.r.rready );

	--Input clock buffer for phy_ref_clk (Also IDELAY clk for DDR)
	buf1: IBUFDS 
	port map (
		o => phy_ref_clk,
		i => clk_p,
		ib => clk_n);
		
	gbe_mac: entity work.gbit_eth_mac 
	port map (
		phy_ref_clk => phy_ref_clk,
		rst => '0',
		mdio => mdio,
		mdic => mdic,
		phy_rstn => phy_rstn,
		bus_clk => bus_clk,
		s_axi_aresetn => aresetn,
		s_axi_aclk => bus_clk,
		s_axi => m_axi);
end	Behavioral;
