ghdl -a --std=08 ../axi4lite_types.vhd
ghdl -a --std=08 ../axi4l_simple_sfr.vhd
ghdl -a --std=08 ../simple_sfr_tb.vhd
ghdl -e --std=08 simple_sfr_tb

./simple_sfr_tb --stop-time=10us --wave=dump.ghw
gtkwave dump.ghw -a wave.gtkw