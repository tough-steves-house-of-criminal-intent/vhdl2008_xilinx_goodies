
library IEEE;
	use IEEE.STD_LOGIC_1164.ALL;
	use IEEE.NUMERIC_STD.ALL;

entity latches is
	Port (
		x : in std_logic := '0';
		dummy : out std_logic_vector(1 downto 0);
		dummy2 : in unsigned(1 downto 0));
end latches;

architecture rtl of latches is
	subtype state_range_t is integer range 0 to 2;
	signal state, nextstate : state_range_t := 0;
begin

	state <= to_integer(dummy2);

	process (state, x)
	begin
		nextstate <= state;

		case state is
			when 0 => if x = '1' then nextstate <= 1; end if;
			when 1 => if x = '0' then nextstate <= 2; end if;
			when 2 => if x = '1' then nextstate <= 0; end if;
		end case;
	end process;

	dummy <= std_logic_vector(to_unsigned(nextstate, 2));
end rtl;