library	IEEE;
	use IEEE.STD_LOGIC_1164.ALL;
	use IEEE.NUMERIC_STD.ALL;
	use IEEE.MATH_REAL.ALL;

library work;
	use work.axi4lite_types.ALL;

entity simple_sfr_tb is

end simple_sfr_tb;

architecture rtl of simple_sfr_tb is
	signal clk : std_logic := '1';
	signal aresetn : std_logic := '0';
	signal s_axi : axi4l_32 := axi4l_32_bus_drive_none;

	signal m_axi_buf : axi4l_m2s_hs := (others => '0');

	signal m_accept : axi4l_hs;
	signal m_set : axi4l_hs;

	signal bus_cycles : unsigned(9 downto 0) := (others => '0');
begin
	clk <= not clk after 5 ns;

	axi4l_assign_m2s_hs(s_axi, m_axi_buf);
	axi4l_assign_m_accept(m_accept, s_axi, m_axi_buf);

	--Very basic master
	--Clear on successful transaction
	process (clk)
	begin
		if rising_edge(clk) then
			if m_accept.aw = '1' then
				m_axi_buf.awvalid <= '0';
			end if;

			if m_accept.ar = '1' then
				m_axi_buf.arvalid <= '0';
			end if;

			if m_accept.r = '1' then
				m_axi_buf.rready <= '0';
			end if;

			if m_accept.w = '1' then
				m_axi_buf.wvalid <= '0';
			end if;

			if m_accept.b = '1' then
				m_axi_buf.bready <= '0';
			end if;

			bus_cycles <= bus_cycles + 1;

			--Signals from stimulus process
			case to_integer(bus_cycles) is
				when 0 =>

				when 1 =>
					aresetn <= '1';

				--Read address followed by delayed read data
				when 5 =>
					s_axi.ar.araddr <= X"00000004";
					m_axi_buf.arvalid <= '1';

				when 7 =>
					m_axi_buf.rready <= '1';

				--Write address and data simultaneous
				when 8 =>
					s_axi.aw.awaddr <= X"00000004";
					m_axi_buf.awvalid <= '1';

					s_axi.w.wdata <= X"ABCD1234";
					s_axi.w.wstrb <= "1000";
					m_axi_buf.wvalid <= '1';

				--Read address and data simultaneous
				when 11 =>
					s_axi.ar.araddr <= X"00000004";
					m_axi_buf.arvalid <= '1';

					m_axi_buf.rready <= '1';

				when 1023 =>
					s_axi <= axi4l_32_bus_drive_none;
					m_axi_buf <= (others => '0');

				when others =>
				
			end case;
		end if;
	end process;

	dut: entity work.axi4l_simple_sfr
		generic map (
			SFR_ADDR => 4,
			SFR_RESET_VALUE => 63)
		port map (
			s_axi_aclk => clk,
			s_axi_aresetn => aresetn,
			s_axi => s_axi);
end rtl;